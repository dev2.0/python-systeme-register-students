from asyncio.windows_events import NULL
from cProfile import label
from cgitb import text
from importlib.resources import contents
from logging import root
from optparse import Values
from textwrap import fill
from tkinter import *
from tkinter import font
from tkinter import ttk
from turtle import bgcolor, title
import mysql.connector


class Studiant:
    def __init__(self, root):
        self.root = root
        self.root.title("Gestion Etudiants")
        self.root.geometry("1350x700+0+0")

        title = Label(self.root, text="Systeme de Gestion Etudiants", font=(
            "Montserrat", 40, "bold"), bg="#88001b", fg="#fff")
        title.pack(side=TOP, fill=X)
# declaration des variables
        self.id_var = StringVar()
        self.nom_var = StringVar()
        self.prenom_var = StringVar()
        self.email_var = StringVar()
        self.tel_var = StringVar()
        self.genre_var = StringVar()

        self.search_by = StringVar()
        self.search_txt = StringVar()

# definition de la primiere case comportant les inputs
        Form_Frame = Frame(self.root, bd=2, relief=RIDGE, bg="#88001b")
        Form_Frame.place(x=20, y=100, width=450, height=580)

# definition des label du formulaire
        form_title = Label(Form_Frame, text="Ajouter un Etudiant", fg="white", font=(
            "Montserrat", 20, "bold"), bg="#88001b")
        form_title.grid(row=0, columnspan=2, padx=20)

# Num
        id_name = Label(Form_Frame, text="ID", fg="white",
                        font=("Montserrat", 20, "bold"), bg="#88001b")
        id_name.grid(row=1, column=0, pady=10, padx=20, sticky="w")

        txt_id_name = Entry(Form_Frame, textvariable=self.id_var, font=(
            "Montserrat", 12), bd=3, relief=GROOVE)
        txt_id_name.grid(row=1, column=1, pady=10, padx=20, sticky="w")
# Nom
        first_name = Label(Form_Frame, text="Nom", fg="white", font=(
            "Montserrat", 20, "bold"), bg="#88001b")
        first_name.grid(row=2, column=0, pady=10, padx=20, sticky="w")

        txt_first_name = Entry(Form_Frame, textvariable=self.nom_var, font=(
            "Montserrat", 12), bd=3, relief=GROOVE)
        txt_first_name.grid(row=2, column=1, pady=10, padx=20, sticky="w")
# Prenom
        last_name = Label(Form_Frame, text="Prenom", fg="white", font=(
            "Montserrat", 20, "bold"), bg="#88001b")
        last_name.grid(row=3, column=0, pady=10, padx=20, sticky="w")

        txt_last_name = Entry(Form_Frame, textvariable=self.prenom_var, font=(
            "Montserrat", 12), bd=3, relief=GROOVE)
        txt_last_name.grid(row=3, column=1, pady=10, padx=20, sticky="w")

# Email
        m_Email = Label(Form_Frame, text="Email", fg="white",
                        font=("Montserrat", 20, "bold"), bg="#88001b")
        m_Email.grid(row=4, column=0, pady=10, padx=20, sticky="w")

        txt_Email = Entry(Form_Frame, textvariable=self.email_var, font=(
            "Montserrat", 12), bd=3, relief=GROOVE)
        txt_Email.grid(row=4, column=1, pady=10, padx=20, sticky="w")
# telephone
        m_tel = Label(Form_Frame, text="Telephone", fg="white",
                      font=("Montserrat", 20, "bold"), bg="#88001b")
        m_tel.grid(row=5, column=0, pady=10, padx=20, sticky="w")

        txt_tel = Entry(Form_Frame, textvariable=self.tel_var, font=(
            "Montserrat", 12), bd=3, relief=GROOVE)
        txt_tel.grid(row=5, column=1, pady=10, padx=20, sticky="w")

# Genre
        gender_box = Label(Form_Frame, text="Genre", fg="white", font=(
            "Montserrat", 20, "bold"), bg="#88001b")
        gender_box.grid(row=6, column=0, pady=10, padx=20, sticky="w")

        combo_gender = ttk.Combobox(Form_Frame, width=18, textvariable=self.genre_var, font=(
            "Montserrat", 12))
        combo_gender['values'] = ("Homme", "Femme")
        combo_gender.grid(row=6, column=1, pady=10, padx=20)
# Adresse
        m_adress = Label(Form_Frame, text="Adress", fg="white",
                         font=("Montserrat", 20, "bold"), bg="#88001b")
        m_adress.grid(row=7, column=0, pady=10, padx=20, sticky="w")

        self.txt_adress = Text(Form_Frame, width=25, height=2, font=(
            "Montserrat", 10), bd=3, relief=GROOVE)
        self.txt_adress.grid(row=7, column=1, pady=10, padx=20, sticky="w")
# les boutons
        btn_Frame = Frame(Form_Frame, relief=RIDGE, bg="#88001b")
        btn_Frame.place(x=10, y=500, width=430)

        btn_inscrire = Button(btn_Frame, text="Inscrire", width=15, pady=5, bg="yellow",
                              command=self.add_students).grid(row=0, column=0, padx=10, pady=10)
        btn_modif = Button(btn_Frame, text="Modifier", width=15, pady=5, bg="yellow",
                           command=self.update_data).grid(row=0, column=1, padx=10, pady=10)
        btn_clear = Button(btn_Frame, text="Clear", width=15, pady=5, bg="yellow",
                           command=self.clear).grid(row=0, column=3, padx=10, pady=10)


# definition de la primiere case comportant l'affichage, les details
        Details_Frame = Frame(self.root, bd=2, relief=RIDGE, bg="#88001b")
        Details_Frame.place(x=500, y=100, width=820, height=580)

# recherche
        m_search = Label(Details_Frame, text="Recherche", fg="white", font=(
            "Montserrat", 16, "bold"), bg="#88001b")
        m_search.grid(row=0, column=0, pady=10, padx=20, sticky="w")

        combo_search = ttk.Combobox(Details_Frame, textvariable=self.search_by, font=(
            "Montserrat", 13, "bold"), width=10, state="readonly")
        combo_search['values'] = ("id", "Nom", "tel")
        combo_search.grid(row=0, column=1, pady=10, padx=20)

        txt_Search = Entry(Details_Frame, textvariable=self.search_txt, width=15, font=(
            "Montserrat", 12, "bold"), bd=3, relief=GROOVE)
        txt_Search.grid(row=0, column=2, pady=10, padx=20, sticky="w")
# btn recherche
        btn_search = Button(Details_Frame, command=self.search_data, bg="yellow", text="Recherche", width=10, pady=5).grid(
            row=0, column=3, padx=10, pady=10)
        btn_afficher = Button(Details_Frame, command=self.fetch_data, bg="yellow", text="Afficher", width=10, pady=5).grid(
            row=0, column=4, padx=10, pady=10)

# Tableau d'affichage
        Tableau_Frame = Frame(Details_Frame, bd=2, relief=RIDGE, bg="#88001b")
        Tableau_Frame.place(x=10, y=70, width=780, height=500)

        scroll_x = Scrollbar(Tableau_Frame, orient=HORIZONTAL)
        scroll_y = Scrollbar(Tableau_Frame, orient=VERTICAL)
        self.Studiant_table = ttk.Treeview(Tableau_Frame, columns=(
            "Id", "Nom", "Prenom", "Email", "Telephone", "Genre", "Adresse"), xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)
        scroll_x.pack(side=BOTTOM, fill=X)
        scroll_y.pack(side=RIGHT, fill=Y)

        scroll_x.config(command=self.Studiant_table.xview)
        scroll_y.config(command=self.Studiant_table.yview)
        self.Studiant_table.heading("Id", text="Id")
        self.Studiant_table.heading("Nom", text="Nom")
        self.Studiant_table.heading("Prenom", text="Prenom")
        self.Studiant_table.heading("Email", text="Email")
        self.Studiant_table.heading("Telephone", text="Telephone")
        self.Studiant_table.heading("Genre", text="Genre")
        self.Studiant_table.heading("Adresse", text="Adresse")

        self.Studiant_table['show'] = 'headings'
        self.Studiant_table.column("Id", width=50)
        self.Studiant_table.column("Nom", width=100)
        self.Studiant_table.column("Prenom", width=100)
        self.Studiant_table.column("Email", width=150)
        self.Studiant_table.column("Telephone", width=120)
        self.Studiant_table.column("Genre", width=100)
        self.Studiant_table.column("Adresse", width=130)
        self.Studiant_table.pack(fill=BOTH, expand=1)

        self.Studiant_table.bind("<ButtonRelease-1>", self.get_cursor)
        self.fetch_data()

# ajouter
    def add_students(self):
        mydb = mysql.connector.connect(
            host="localhost", username="root", password="", database="projet_python_assane_gueye")
        mycursor = mydb.cursor()
        mycursor.execute("insert into users(id,nom, prenom, email, tel, genre, adress) values(%s,%s,%s,%s,%s,%s,%s)", (
            self.id_var.get(),
            self.nom_var.get(),
            self.prenom_var.get(),
            self.email_var.get(),
            self.tel_var.get(),
            self.genre_var.get(),
            self.txt_adress.get('1.0', END)
        ))
        mydb.commit()
        self.fetch_data()
        self.clear()
        mydb.close()

    def fetch_data(self):
        mydb = mysql.connector.connect(
            host="localhost", username="root", password="", database="projet_python_assane_gueye")
        mycursor = mydb.cursor()
        mycursor.execute("select * from users")
        rows = mycursor.fetchall()
        if len(rows) != 0:
            self.Studiant_table.delete(*self.Studiant_table.get_children())
            for row in rows:
                self.Studiant_table.insert('', END, values=row)
            mydb.commit()
        mydb.close()

    def clear(self):
        self.id_var.set(""),
        self.nom_var.set(""),
        self.prenom_var.set(""),
        self.email_var.set(""),
        self.tel_var.set(""),
        self.genre_var.set(""),
        self.txt_adress.delete('1.0', END)

    def get_cursor(self, ev):
        cursor_row = self.Studiant_table.focus()
        contents = self.Studiant_table.item(cursor_row)
        row = contents['values']
        self.id_var.set(row[0]),
        self.nom_var.set(row[1]),
        self.prenom_var.set(row[2]),
        self.email_var.set(row[3]),
        self.tel_var.set(row[4]),
        self.genre_var.set(row[5]),
        self.txt_adress.delete('1.0', END)
        self.txt_adress.insert(END, row[6])

    def update_data(self):
        mydb = mysql.connector.connect(
            host="localhost", username="root", password="", database="projet_python_assane_gueye")
        mycursor = mydb.cursor()
        mycursor.execute("update users set nom=%s, prenom=%s, email=%s, tel=%s, genre=%s, adress=%s WHERE id=%s", (
            self.nom_var.get(),
            self.prenom_var.get(),
            self.email_var.get(),
            self.tel_var.get(),
            self.genre_var.get(),
            self.txt_adress.get('1.0', END),
            self.id_var.get()
        ))

        mydb.commit()
        self.fetch_data()
        self.clear()
        mydb.close()

    def search_data(self):
        mydb = mysql.connector.connect(
            host="localhost", username="root", password="", database="projet_python_assane_gueye")
        mycursor = mydb.cursor()

        mycursor.execute("select * from users where " + str(self.search_by.get()
                                                            ) + " Like '%"+str(self.search_txt.get())+"%'")
        rows = mycursor.fetchall()
        if len(rows) != 0:
            self.Studiant_table.delete(*self.Studiant_table.get_children())
            for row in rows:
                self.Studiant_table.insert('', END, values=row)
            mydb.commit()
        mydb.close()


root = Tk()
ob = Studiant(root)
root.mainloop()
